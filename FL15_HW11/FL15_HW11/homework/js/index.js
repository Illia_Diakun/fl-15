import '../css/style.css';

function History() {
    const history = {
        firstPlayer: 0,
        secondPlayer: 0
    };

    this.get = function() {
        return history;
    }

    this.reset = function() {
        history.firstPlayer = 0;
        history.secondPlayer = 0;
    }

    this.update = function(firstPlayer, secondPlayer) {
        history.firstPlayer += firstPlayer;
        history.secondPlayer += secondPlayer;
    }
}

function clearTable() {
    const table = document.querySelector('.table');
    table.innerHTML = '';
    for (let i = 0; i < 3; i++) {
        const tr = document.createElement('tr');
        for (let j = 0; j < 3; j++) {
            const td = document.createElement('td');
            td.classList.add('cell');
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
}

const directions = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];

const history = new History();

function newGame() {
    document.querySelector('table').classList.remove('disabled');
    clearTable();
    let isFirstPlayerTurn = true;
    const board = ['empty', 'empty', 'empty', 'empty', 'empty', 'empty', 'empty', 'empty', 'empty'];
    const tableCells = [...document.querySelectorAll('.cell')];
    tableCells.forEach((el, index) => {
        el.addEventListener('click', () => {
            mark(index, board, isFirstPlayerTurn);
            el.classList.add(isFirstPlayerTurn ? 'cross' : 'zero');
            checkBoard(board, tableCells);
            isFirstPlayerTurn = !isFirstPlayerTurn;
        });
    });
}

function mark(index, board, isFirstPlayerTurn) {
    board[index] = isFirstPlayerTurn ? 'x' : 'o';
}

function checkBoard(board, tableCells) {
    directions.forEach((direction) => {
        const str = board[direction[0]] + board[direction[1]] + board[direction[2]];
        if (str === 'xxx') {
            finishGame(direction, tableCells, 'first');
            return;
        } else if (str === 'ooo') {
            finishGame(direction, tableCells, 'second');
            return;
        }
    });
    const isEmptyField = board.find((cell) => cell === 'empty');
    if (!isEmptyField) {
        finishGame(null, tableCells, 'draw');
    }
}

function finishGame(direction, tableCells, winner) {
    document.querySelector('table').classList.add('disabled');
    if (winner === 'draw') {
        history.update(1, 1);
        tableCells.forEach((el) => el.classList.add('draw'));
    } else {
        direction.forEach((i) => tableCells[i].classList.add('win'));
        if (winner === 'first') {
            history.update(1, 0);
        } else {
            history.update(0, 1);
        }
    }
    updateHistory();
}

function reset() {
    history.reset();
    updateHistory();
    newGame();
}

function updateHistory() {
    const players = document.querySelectorAll('.player');
    players[0].innerText = `Player 1 (${history.get().firstPlayer})`;
    players[1].innerText = `Player 2 (${history.get().secondPlayer})`;
}

function init() {
    document.getElementById('newGame').addEventListener('click', newGame);
    document.getElementById('reset').addEventListener('click', reset);
}

init();
