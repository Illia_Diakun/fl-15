const _suit = Symbol('suit');
const _rankList = Symbol('ranklist');

class Card {
    constructor(rank, suit) {
        this.rank = rank;
        this[_suit] = suit;
        this[_rankList] = ['Ace', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King'];
    }

    get isFaceCard() {
        const one = 1;
        const ten = 10;
        return !!(this.rank === one || this.rank > ten);
    }

    toString() {
        return `${this[_rankList][this.rank - 1]} of ${this[_suit]}`;
    }

    static compare(cardOne, cardTwo) {
        const one = 1;
        const negativeOne = -1;
        if (cardOne.rank > cardTwo.rank) {
            return one;
        } else if (cardOne.rank < cardTwo.rank) {
            return negativeOne;
        } else {
            return 0;
        }
    }
}

const _cards = Symbol('cards');
const _count = Symbol('count');

class Deck {
    constructor() {

        const thirteen = 13;
        const three = 3;
        this[_cards] = (function() {
            const suit = ['Hearts', 'Diamonds', 'Clubs', 'Spades'];
            let cards = [];

            for (let i = 1; i <= thirteen; i++) {
                for (let j = 0; j <= three; j++) {
                    cards.push(new Card(i, suit[j]));
                }
            }

            return cards;
        })();

        this[_count] = this[_cards].length; 
    }

    get count() {
        return this[_count];
    }

    draw(n) {
        this[_count] -= n;
        return this[_cards].splice(this[_cards].length - n, n);
    }

    shuffle() {
        let tempCard;
        let tempIndex;

        for (let i = 0; i < this[_count]; i++) {
            tempIndex = Math.floor(Math.random() * this.count);
            tempCard = this[_cards][tempIndex];
            this[_cards][tempIndex] = this[_cards][i];
            this[_cards][i] = tempCard;
        }
    }
}

const _wins = Symbol('wins');

class Player {
    constructor(name) {
        this.name = name;
        this.deck = null;
        this[_wins] = 0;
    }

    get wins() {
        return this[_wins];
    }

    addWin() {
        ++this[_wins];
    }

    static play(playerOne, playerTwo) {
        playerOne.deck = new Deck();
        playerTwo.deck = new Deck();

        playerOne.deck.shuffle();
        playerTwo.deck.shuffle();

        while (playerOne.deck.count > 0) {
            let cardOne = playerOne.deck.draw(1);
            let cardTwo = playerTwo.deck.draw(1);
            let result = Card.compare(cardOne[0], cardTwo[0]);

            console.log(`${cardOne[0].toString()} vs ${cardTwo[0].toString()}`);
            const negativeOne = -1;
            if (result === 1) {
                playerOne.addWin();
            } else if (result === negativeOne) {
                playerTwo.addWin();
            }
        }

        if (playerOne.wins > playerTwo.wins) {
            console.log(`${playerOne.name} wins ${playerOne.wins} to ${playerTwo.wins}`);
        } else if (playerOne.wins < playerTwo.wins) {
            console.log(`${playerTwo.name} wins ${playerTwo.wins} to ${playerOne.wins}`);
        } else {
            console.log(`A drow ${playerOne.wins} to ${playerTwo.wins}`);
        }
    }
}

const playerOne = new Player('John');
const playerTwo = new Player('Peter');
Player.play(playerOne, playerTwo);

const employees = [];

class Employee {
    
    constructor(emp) {
        this.id = emp.id;
        this.firstName = emp.firstName;
        this.lastName = emp.lastName;
        this.birthday = emp.birthday;
        this.salary = emp.salary;
        this.position = emp.position;
        this.departament = emp.departament;
        Employee.EMPLOYEES.push(this);
    }

    get age() {
        const today = new Date();
        const birthday = new Date(this.birthday);
        let years = today.getFullYear() - birthday.getFullYear();
        let months = today.getMonth() - birthday.getMonth();
        let days = today.getDate() - birthday.getDate();
        if (months > 0 || months === 0 && days >= 0) {
            return years;
        } else {
            return --years;
        }
    }

    get fullName() {
        return `${this.firstName} ${this.lastName}`;
    }

    static get EMPLOYEES() {
        return employees;
    }

    quit() {
        Employee.EMPLOYEES.splice(Employee.EMPLOYEES.indexOf(this), 1);
    }

    retire() {
        console.log('It was such a pleasure to work with you!');
        this.quit();
    }

    getFired() {
        console.log('Not a big deal!');
        this.quit();
    }

    changeDepartament(newDepartament) {
        this.departament = newDepartament;
    }

    changePosition(newPosition) {
        this.position = newPosition;
    }

    changeSalary(newSalary) {
        this.salary = newSalary;
    }

    getPromoted(benefits) {
        if (benefits.salary) {
            this.changeSalary(benefits.salary);
        }
        if (benefits.position) {
            this.changePosition(benefits.position);
        }
        if (benefits.departament) {
            this.changeDepartament(benefits.departament);
        }

        console.log('Yoohooo!');
    }

    getDemoted(punishment) {
        if (punishment.salary) {
            this.changeSalary(punishment.salary);
        } 
        if (punishment.position) {
            this.changePosition(punishment.position);
        }        
        if (punishment.department) {
            this.changeDepartment(punishment.department);
        }

        console.log('Damn!');
    }
}

class Manager extends Employee {
    constructor(emp) {
        super(emp);
        this.changePosition('manager');
    }

    get managedEmployees() {
        let result = [];
        for (let i = 0; i < Employee.EMPLOYEES.length; i++) {
            if (Employee.EMPLOYEES[i].department === this.department && Employee.EMPLOYEES[i].position !== 'manager') {
                result.push(Employee.EMPLOYEES[i]);
            }
        }

        return result;
    }
}

class BlueCollarWorker extends Employee {}

class HRManager extends Manager {
    constructor (emp) {
        super(emp);
        this.changeDepartment('hr');
    }
}

class SalesManager extends Manager {
    constructor (emp) {
        super(emp);
        this.changeDepartment('sales');
    }
}

const promoter = {
    promote(id, benefits) {
        this.managedEmployees.find((empl) => empl.id === id).getPromoted(benefits);
    }
}

const demoter = {
    demote(id, punishment) {
        this.managedEmployees.find((empl) => empl.id === id).getDemoted(punishment);
    }
}

function ManagerPro(manager, ...args) {
    return Object.assign(manager, ...args);
}

