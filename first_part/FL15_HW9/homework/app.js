const ten = 10;
const eighteen = 18;
function reverseNumber(num) {
    let str = num >= 0 ? '' : '-';
    let newNum = Math.abs(num);
    while (newNum > 0) {
        str += (newNum % ten).toString();
        newNum = Math.trunc(newNum / ten)
    }
    return Number(str);
}


function forEach(arr, func) {
    for (let i = 0; i < arr.length; i++) {
        func(arr[i]);
    }
}

function map(arr, func) {
    const newArr = [];
    for (let i = 0; i < arr.length; i++) {
        newArr.push(func(arr[i]));
    }
    return newArr;
}

function filter(arr, func) {
    const newArr = [];
    forEach(arr, function (el) {
        if (func(el)) {
            newArr.push(el);
        }
    })
    return newArr;
}

function getAdultAppleLovers(data) {
    const result = filter(data, person => {
        return person.age > eighteen && person.favoriteFruit === 'apple';
    })
    return map(result, personName => personName.name);
}

function getKeys(obj) {
    const array = [];
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            array.push(key);
        }
    }

    return array;
}

function getValues(obj) {
    const array = [];
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            array.push(obj[key]);
        }
    }

    return array;
}

function showFormattedDate(dateObj) {
    const monthNames = [
        'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
    ];
    return `Date: ${dateObj.getDate()} of ${monthNames[dateObj.getMonth()]}, ${dateObj.getFullYear()}`
}
showFormattedDate(new Date('2018-08-27T01:10:00'));
