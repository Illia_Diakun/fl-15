console.log('Heap sort');

function heapSort(arr){
    let len = arr.length,
        end = len-1;

    heapify(arr, len);

    while(end > 0){
        swap(arr, end--, 0);
        siftDown(arr, 0, end);
    }
    return arr;
}
function heapify(arr, len){
    let mid = Math.floor((len-two)/two);
    while(mid > 0){
        siftDown(arr, --mid, len-1);
    }
}
function siftDown(arr, start, end){
    let root = start,
        child = root*two+1,
        toSwap = root;
    while(child <= end){
        if(arr[toSwap] < arr[child]){
            swap(arr, toSwap, child);
        }
        if(child+1 <= end && arr[toSwap] < arr[child+1]){
            swap(arr, toSwap, child+1)
        }
        if(toSwap !== root){
            swap(arr, root, toSwap);
            root = toSwap;
        } else{
            return;
        }
        toSwap = root;
        child = root*two+1
    }
}

function swap(arr, i, j){
    let temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}

const one = 1, two = 2, three = 3, four = 4, five = 5, six = 6, seven = 7, nine = 9;

console.log(heapSort([seven,five,two,four,three,nine])); //[2, 3, 4, 5, 7, 9]
console.log(heapSort([nine,seven,five,four,three,one])); //[1, 3, 4, 5, 7, 9]
console.log(heapSort([one,two,three,four,five,six])); //[1, 2, 3, 4, 5, 6]