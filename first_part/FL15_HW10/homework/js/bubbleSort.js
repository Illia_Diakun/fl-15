console.log('Bubble sort');
function bubbleSort(arr){
    let len = arr.length;
    for (let i = len-1; i>=0; i--){
        for(let j = 1; j<=i; j++){
            if(arr[j-1]>arr[j]){
                let temp = arr[j-1];
                arr[j-1] = arr[j];
                arr[j] = temp;
            }
        }
    }
    return arr;
}

const one = 1, two = 2, three = 3, four = 4, five = 5, six = 6, seven = 7, nine = 9;

console.log(bubbleSort([seven,five,two,four,three,nine])); //[2, 3, 4, 5, 7, 9]
console.log(bubbleSort([nine,seven,five,four,three,one])); //[1, 3, 4, 5, 7, 9]
console.log(bubbleSort([one,two,three,four,five,six])); //[1, 2, 3, 4, 5, 6]
