const two = 2;
const timeRegExp = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/;
const mes = prompt('Input event name', 'meeting');
const form = document.forms[0];
const formCont = document.querySelector('.container');

if (mes) {
    formCont.classList.remove('hidden');
}

function isValidNumber(num) {
    return isFinite(num) && num > 0;
}

function isValidTime(time) {
    return timeRegExp.test(time);
}

function converter() {
    const euro = Number(prompt('Input amount of euro', '0'));
    const dollar = Number(prompt('Input amount of dollar', '0'));
    const euroHrn = 32.93;
    const dollarHrn = 27.7;

    if (isValidNumber(euro) && isValidNumber(dollar)) {
        alert(`${euro.toFixed(two)} euros are equal ${(euro * euroHrn).toFixed(two)} hrns,
${dollar.toFixed(two)} dollars are equal ${(dollar * dollarHrn).toFixed(two)} hrns`);
    }
}

function confirmMeeting() {
    const userName = document.getElementById('name').value;
    const time = document.getElementById('time').value;
    const place = document.getElementById('place').value;
    
    if (!userName || !time || !place) {
        alert('Input all data');
    } else if (!isValidTime(time)) {
        alert('Enter time in format hh:mm');
    } else {
        console.log(`${userName} has a ${mes} today at ${time} somewhere in ${place}`);
    }
}