const appRoot = document.getElementById('app-root');

const header = document.createElement('header');
header.innerHTML = `
<h1 class="title">Countries Search</h1>
<div id="searchType" class="searchType">
    <p>Please choose type of search:</p>
    <div>
        <div>
            <input type="radio" id="region" name="searchType" value="region">
            <label for="region">By Region</label>
        </div>
        <div>
            <input type="radio" id="language" name="searchType" value="language">
            <label for="language">By Language</label>
        </div>
    </div>
</div>
<div id="searchValue" class="searchValue">
    <p>Please choose search query:</p>
    <select name="select" id="select" disabled>
        <option value="">Select value</option>
    </select>
</div>
`
appRoot.append(header);

const searchType = document.getElementById('searchType');
const searchValue = document.getElementById('searchValue');
const region = document.getElementById('region');
const language = document.getElementById('language');

searchType.addEventListener('click', e => {
    if (e.target === region || e.target === language) {
        removeElem('select');
        const newSelect = document.createElement('select');
        newSelect.setAttribute('name', 'select');
        newSelect.id = 'select';
        newSelect.innerHTML = '<option value="">Select value</option>'

        if (e.target === region) {
            const regionList = externalService.getRegionsList();

            for (let i = 0; i < regionList.length; i++) {
                const newOption = document.createElement('option');
                newOption.setAttribute('value', regionList[i]);
                newOption.textContent = regionList[i];
                newSelect.append(newOption);
            }
        }
        if (e.target === language) {
            const languageList = externalService.getLanguagesList();
        
            for (let i = 0; i < languageList.length; i++) {
                const newOption = document.createElement('option');
                newOption.setAttribute('value', languageList[i]);
                newOption.textContent = languageList[i];
                newSelect.append(newOption);
            }
        }
        searchValue.append(newSelect);

        const p = document.createElement('p');
        p.id = 'info';
        p.textContent = 'No items, please choose search query';

        if (!document.getElementById('info')) {
            appRoot.append(p);    
        }

        if (document.getElementById('table')) {
            removeElem('table')
        }

        newSelect.addEventListener('change', () => {
            if (newSelect.value === '' && !document.getElementById('info')) {
                appRoot.append(p);
                if (document.getElementById('table')) {
                    removeElem('table');  
                }
            } else if (newSelect.value !== '' && document.getElementById('info')) {
                removeElem('info');
            }

            if (document.getElementById('table')) {
                removeElem('table');  
            }
        
            let countryList = [];

            if (region.checked) {
                countryList = externalService.getCountryListByRegion(newSelect.value);
            } else {
                countryList = externalService.getCountryListByLanguage(newSelect.value);
            }  

            if (countryList.length !== 0) {
                addTable(countryList);
            }
            const btns = document.querySelectorAll('button');
            const table = document.getElementById('table');

            table.addEventListener('click', e => {
                if (e.target === btns[0]) {
                    removeElem('tbody');
                    btns[1].innerHTML = '&#8597;';
                    if (btns[0].innerHTML === '↑') {
                        btns[0].innerHTML = '&#8595;';
                        countryList = sortArray(countryList, 'name', 'down');
                    } else {
                        btns[0].innerHTML = '&#8593;';
                        countryList = sortArray(countryList, 'name', 'up');
                    }
                    addTBody(table, countryList);
                } else if (e.target === btns[1]) {
                    removeElem('tbody');
                    btns[0].innerHTML = '&#8597;';
                    if (btns[1].innerHTML === '↑') {
                        btns[1].innerHTML = '&#8595;';
                        countryList = sortArray(countryList, 'area', 'down');
                    } else {
                        btns[1].innerHTML = '&#8593;';
                        countryList = sortArray(countryList, 'area', 'up');
                    }
                    addTBody(table, countryList);
                }
            })
        });
    }
});

function removeElem(id) {
    document.getElementById(id).remove();
}

function addTable(list) {
    const table = document.createElement('table');
    const thead = document.createElement('thead');
    const thr = document.createElement('tr');
                    
    const listThead = ['Country name', 'Capital', 'World Region', 'Languages', 'Area', 'Flag'];
    
    table.id = 'table';

    for (let i = 0; i < listThead.length; i++) {
        const zero = 0, four = 4;
        const th = document.createElement('th');
        th.innerText = listThead[i];
        if (i === zero || i === four) {
            const btn = document.createElement('button');
            btn.innerHTML = '&#8597;';
            th.append(btn);
        }
        thr.append(th);
    }
    
    thead.append(thr);
    table.append(thead);
    addTBody(table, list);
    appRoot.append(table);

    table.addEventListener('mouseover', e => {
        let curRow = -1;
        for (let i = 0; i < table.rows.length; i++) {
            for (let j = 0; j < table.rows[i].cells.length; j++) {
                if (e.target === table.rows[i].cells[j]) {
                    curRow = i;
                }
            }
        }
        if (curRow > 0) {
            for (let i = 1; i < table.rows.length; i++) {
                if (table.rows[i].classList.contains('gray')) {
                    table.rows[i].classList.remove('gray');
                }
            }

            table.rows[curRow].classList.add('gray');
        }
    })
}

function addTBody(root, list) {
    const tbody = document.createElement('tbody');
    tbody.id = 'tbody';

    for (let i = 0; i < list.length; i++) {
        const tr = document.createElement('tr');
        const six = 6;
        for (let j = 1; j <= six; j++) {
            const td = document.createElement('td');
            const two = 2, three = 3, four = 4, five = 5, six = 6;
            switch (j) {
                case 1:
                    td.textContent = list[i].name;
                    tr.append(td);
                    break;
                case two:
                    td.textContent = list[i].capital;
                    tr.append(td);
                    break;
                case three:
                    td.textContent = list[i].region;
                    tr.append(td);
                    break;
                case four:
                    td.textContent = Object.values(list[i].languages).join(', ');
                    tr.append(td);
                    break;
                case five:
                    td.textContent = list[i].area;
                    tr.append(td);
                    break;
                case six:
                    td.innerHTML = `<img src=${list[i].flagURL} alt="">`;
                    tr.append(td);
                    break;
                default:
                    break;
            }
        }
        tbody.append(tr);
    }
    root.append(tbody);
}

function sortArray(arr, prop, direc) {
    if (direc === 'up') {
        let minIdx, temp,
        len = arr.length;
        for(let i = 0; i < len; i++){
            minIdx = i;
            for(let j = i+1; j<len; j++){
                if(arr[j][prop]<arr[minIdx][prop]){
                    minIdx = j;
                }
            }
            temp = arr[i];
            arr[i] = arr[minIdx];
            arr[minIdx] = temp;
        }
        return arr;
    } else if (direc === 'down') {
        let minIdx, temp,
        len = arr.length;
        for(let i = 0; i < len; i++){
            minIdx = i;
            for(let j = i+1; j<len; j++){
                if(arr[j][prop]>arr[minIdx][prop]){
                    minIdx = j;
                }
            }
            temp = arr[i];
            arr[i] = arr[minIdx];
            arr[minIdx] = temp;
        }
        return arr;
    }
}