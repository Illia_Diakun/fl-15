function isEqual(a, b) {
    return a === b;
}

function isBigger(a, b) {
    return a > b;
}

function storeNames() {
    const arr = [];
    for (let i = 0; i < arguments.length; i++) {
        arr.push(arguments[i]);
    }
    return arr;
}

function getDifference(a, b) {
    return a > b ? a - b : b - a;
}

function negativeCount(arr) {
    return arr.reduce((acc, cur) => cur < 0 ? ++acc : acc, 0);
}

function letterCount(str1, str2) {
    return str1.split('').reduce((acc, val) => val === str2 ? ++acc : acc, 0)
}

function countPoints(arr) {
    return arr.map(el => el.split(':')).reduce((acc, val) => {
        const three = 3;
        if (+val[0] > +val[1]) {
            return acc + three
        } else if (+val[0] === +val[1]) {
            return ++acc
        } else {
            return acc
        }
    }, 0);
}

