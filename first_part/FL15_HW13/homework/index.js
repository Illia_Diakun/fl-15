function getAge(dateObj){
    const today = new Date();
    const birthday = new Date(dateObj);
    if (today.getMonth() < birthday.getMonth() || 
        today.getMonth() === birthday.getMonth() && today.getDate() < birthday.getDate()) {
        return today.getFullYear() - birthday.getFullYear() - 1
    } else {
        return today.getFullYear() - birthday.getFullYear()
    }
}

function getWeekDay(dateObj) {
    const week = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const date = new Date(dateObj);
    return week[date.getDay()];
}

function getAmountDaysToNewYear() {
    const today = new Date();
    const newYear = new Date(today.getFullYear() + 1, 0, 1);
    const miliseconds = 86400000;
    return Math.ceil((newYear - today) / miliseconds);
}

function getProgrammersDay(year) {
    const fourHundred = 400;
    const hundred = 100;
    const twelve = 12;
    const thirteen = 13;
    const eight = 8;
    const four = 4;
    const date = new Date(year, 0, 1);
    if (year % fourHundred === 0 || year % hundred !== 0 && year % four === 0) {
        return `12 Sep, ${date.getFullYear()} (${getWeekDay(new Date(year, eight, twelve))})`
    } else {
        return `13 Sep, ${date.getFullYear()} (${getWeekDay(new Date(year, eight, thirteen))})`
    }
}

function howFarIs(str) {
    const seven = 7;
    const week = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const today = new Date();
    const specifiedWeekday = str.charAt().toUpperCase() + str.slice(1).toLowerCase();
    const todayIndex = today.getDay();
    const specIndex = week.findIndex(el => el === specifiedWeekday);
    const number = todayIndex < specIndex ? specIndex - todayIndex : specIndex - todayIndex + seven;
    if (todayIndex === specIndex) {
        return `Hey, today is ${specifiedWeekday} =)`
    } else {
        return `It's ${number} day(s) left till ${specifiedWeekday}`
    }
}

function isValidIdentifier(str) {
    const reg = /[a-zA-Z_$][\w$]*/g;
    const arrOfStr = str.match(reg);
    return arrOfStr ? arrOfStr[0] === str : false;
}

function capitalize(str) {
    return str.replace(/\b\w/gi, str => str[0].toUpperCase());
}

function isValidAudioFile(file) {
    const reg = /[a-zA-Z]+\.(mp3|flac|alac|aac)/;
    const arrOfStr = file.match(reg);
    return arrOfStr ? arrOfStr[0] === file : false;
}

function getHexadecimalColors(str) {
    const reg = /#([a-f0-9]{6}|[a-f0-9]{3})\b/gi;
    const arrOfStr = str.match(reg);
    return arrOfStr || [];
}

function isValidPassword(str) {
    const negOne = -1;
    const reg = /[a-zA-Z0-9]{8,}/g;
    const arrOfStr = str.match(reg);
    const ifMatch = arrOfStr ? arrOfStr[0] === str : false;
    if (str.search(/[a-z]/) === negOne || str.search(/[A-Z]/) === negOne || str.search(/\d/) === negOne || !ifMatch) {
        return false;
    } else {
        return true;
    }
}

function addThousandsSeparators(number) {
    let numberParts = number.toString().split('.');
    numberParts[0] = numberParts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return numberParts.join('.');
}

function getAllUrlsFromText(str) {
    const reg = /(http|https):\/\/[a-z0-9]+(\.[a-z0-9]+)+\//gi;
    const arrOfStr = str.match(reg);
    return arrOfStr || [];
}