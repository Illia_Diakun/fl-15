const root = document.getElementById('root');

window.onhashchange = onHashChange;

const tweets = [];

let currentEditingId = null;

const tweetsList = document.getElementById('list');
const addTweetButton = document.querySelector('#navigationButtons .addTweet');
addTweetButton.addEventListener('click', () => {
    location.hash = '/add';
});
const modifyItem = document.getElementById('modifyItem');
const modifyItemHeader = document.getElementById('modifyItemHeader');
const modifyItemInput = document.getElementById('modifyItemInput');

const cancelModificationButton = document.getElementById('cancelModification');
cancelModificationButton.addEventListener('click', cancelModification);

const saveChangesButton = document.getElementById('saveModifiedItem');
saveChangesButton.addEventListener('click', saveChanges);

modifyItemInput.addEventListener('change', updateSaveChangesButton);

const goToLikedButton = document.createElement('button');
goToLikedButton.innerText = 'Go To Liked';
goToLikedButton.addEventListener('click', () => {
    location.hash = 'liked';
});
document.getElementById('navigationButtons').appendChild(goToLikedButton);

const backToAllButton = document.createElement('button');
backToAllButton.innerText = 'Back';
backToAllButton.classList.add('hidden');
backToAllButton.addEventListener('click', () => {
    location.hash = '';
});
document.getElementById('navigationButtons').appendChild(backToAllButton);

function onHashChange() {
    console.log(location.hash);
    const two = 2;
    if (location.hash.includes('edit')) {
        modifyItemHeader.innerText = 'Edit tweet';
        modifyItem.classList.remove('hidden');
        tweetsList.classList.add('hidden');
        currentEditingId = +location.hash.split('/')[two];
    } else if (location.hash === '#/add') {
        modifyItemHeader.innerText = 'Add tweet';
        modifyItem.classList.remove('hidden');
        tweetsList.classList.add('hidden');
        updateSaveChangesButton();
    } else if (!location.hash || location.hash === '#liked') {
        modifyItem.classList.add('hidden');
        tweetsList.classList.remove('hidden');
        if (location.hash === '#liked') {
            backToAllButton.classList.remove('hidden');
            goToLikedButton.classList.add('hidden');
            addTweetButton.classList.add('hidden');
        } else {
            backToAllButton.classList.add('hidden');
            goToLikedButton.classList.remove('hidden');
            addTweetButton.classList.remove('hidden');
        }
        renderTweets();
    } else {
        location.hash = '';
    }
}

function updateSaveChangesButton() {
    const hundredforty = 140;
    saveChangesButton.disabled = !modifyItemInput.value || modifyItemInput.value.length > hundredforty;
}

function cancelModification() {
    modifyItemInput.value = '';
    location.hash = '';
    currentEditingId = null;
}

function saveChanges() {
    if (currentEditingId) {
        editTweet();
    } else {
        addTweet();
    }
    modifyItemInput.value = '';
    location.hash = '';
    currentEditingId = null;
}

function editTweet() {
    const tweet = tweets.find(tweet => tweet.id === currentEditingId);
    tweet.text = modifyItemInput.value;
}

function addTweet() {
    const maxId = Math.max(...tweets.map(tweet => tweet.id), 0);
    tweets.push({
        text: modifyItemInput.value,
        liked: false,
        id: maxId + 1
    });
}

function removeTweet(id) {
    const index = tweets.findIndex(tweet => tweet.id === id);
    tweets.splice(index, 1);
    renderTweets();
}

function likeTweet(id) {
    const tweet = tweets.find(tweet => tweet.id === id);
    tweet.liked = !tweet.liked;
    renderTweets();
}

function renderTweets() {
    tweetsList.innerHTML = '';
    const currentTweets = location.hash === '#liked' ? tweets.filter(tweet => tweet.liked) : tweets;
    currentTweets.forEach((tweet) => {
        const newTweet = document.createElement('li');
        const newTweetText = document.createElement('p');
        newTweetText.innerText = tweet.text;
        newTweetText.addEventListener('click', () => {
            location.hash = `/edit/${tweet.id}`;
        });
        newTweet.appendChild(newTweetText);
        const likeButton = document.createElement('button');
        likeButton.innerText = tweet.liked ? 'Unlike' : 'Like';
        likeButton.addEventListener('click', () => {
            likeTweet(tweet.id);
        });
        newTweet.appendChild(likeButton);
        const removeButton = document.createElement('button');
        removeButton.innerText = 'Remove';
        removeButton.addEventListener('click', () => {
            removeTweet(tweet.id);
        })
        newTweet.appendChild(removeButton);
        tweetsList.appendChild(newTweet);
    });
}

