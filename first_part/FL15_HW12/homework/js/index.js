const initialCounters = {
	Page1: 0,
	Page2: 0,
	Page3: 0
};

function visitLink(path) {
	const counters = JSON.parse(localStorage.getItem('counters'));
	counters[path]++;
	localStorage.setItem('counters', JSON.stringify(counters));
}

function viewResults() {
	const counters = JSON.parse(localStorage.getItem('counters'));
	const content = document.getElementById('content');
	const isVisitedPage = Object.values(counters).some(function (counter) {
		return counter > 0;
	});
	if (isVisitedPage) {
		const list = document.createElement('ul');
		Object.entries(counters).forEach(function (counter) {
			if (counter[1] > 0) {
				const listItem = document.createElement('li');
				listItem.innerText = `You visited ${counter[0]} ${counter[1]} time(s)`;
				list.appendChild(listItem);
			}
		});
		content.appendChild(list);
	} else {
		const message = document.createElement('p');
		message.innerText = 'No pages were visited';
		content.appendChild(message);
	}
	localStorage.setItem('counters', JSON.stringify(initialCounters));
}

function initialize() {
	if (!localStorage.getItem('counters')) {
		localStorage.setItem('counters', JSON.stringify(initialCounters));
	}
}

initialize();