const attempts = 3;
const min = 0;
const max = 8;
const startPrize = 100;
const two = 2;
const four = 4;
let playGame = confirm('Do you want to play a game?');
if (!playGame) {
    alert('You did not become a billionaire, but can.')
} else {
    let isGame = true;
    let currentMin = min;
    let currentMax = max;
    let currentStartPrize = startPrize;
    let totalPrize = 0;
    while (isGame) {
        let randomNumber = Math.floor(Math.random() * (currentMax - currentMin)) + currentMin;
        console.log(randomNumber);
        let isGuessed = false;
        let currentAttempts = attempts;
        let currentAttemptPrize = currentStartPrize;
        while (currentAttempts > 0) {
            const guess = parseInt(prompt(
                                 `Enter a number of pocket on which the ball could land from 0 to ${currentMax}
                                  Attempts left: ${currentAttempts}
                                  Total prize: ${totalPrize}
                                  Possible prize on current attempt: ${currentStartPrize}`
            ));
            if (randomNumber === guess) {
                const isContinue = confirm(`Congratulation, you won! Your prize is: ${currentStartPrize}$.
                Do you want to continue?`);
                if (isContinue) {
                    totalPrize += currentAttemptPrize;
                    currentStartPrize *= two;
                    currentMax += four;
                    isGuessed = true;
                }
                break;
            } else {
                currentAttemptPrize /= two;
                currentAttempts--;
            }
        }
        if (!isGuessed) {
            alert(`Thank you for your participation. Your prize is: ${totalPrize} $`);
            const isPlayAgain = confirm(`Do you want to play again?`)
            if (isPlayAgain) {
                totalPrize = 0;
                currentStartPrize = startPrize;
                currentMax = max;
            } else {
                isGame = false;
            }
        }
    }
}


