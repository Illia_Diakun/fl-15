function profit() {
    const thousand = 1000;
    const hundred = 100;
    const two = 2;
    const initialAmount = parseInt( prompt('User inputs initial amount of money', 0));
    if (isNaN(initialAmount) || initialAmount < thousand) {
        alert('Invalid input data');
        return;
    }
    let numberOfYears = parseInt(prompt('User inputs number of years', 0));
    if(isNaN(numberOfYears) || numberOfYears < 1) {
        alert('invalid input data');
        return;
    }
    const percentageOfYears = parseFloat(prompt('User inputs percentage of a year', 0));
    if(isNaN(percentageOfYears) || percentageOfYears > hundred) {
    alert('Invalid input data');
    return;
    }
    const totalAmount = initialAmount * Math.pow(1 + percentageOfYears/hundred, numberOfYears);
    const totalProfit = totalAmount - initialAmount;
    const totalProfitStr = Number.isInteger(totalProfit) ? totalProfit.toString() : totalProfit.toFixed(two);
    const totalAmountStr = Number.isInteger(totalAmount) ? totalAmount.toString() : totalAmount.toFixed(two);
    const message = `Initial Amount: ${initialAmount}
                     Number of Years: ${numberOfYears}
                     Percentage of years: ${percentageOfYears}%
                     Total profit: ${totalProfitStr}
                     Total amount: ${totalAmountStr}`;
    alert(message);
}
profit();

