const two = 2;
const three = 3;
const table = document.querySelector('table');
const specCell = table.rows[1].cells[two];
table.addEventListener('click', function(e) {
    let curRow = -1;
    let curCell = -1;
    for (let i = 0; i < three; i++) {
        for (let j = 0; j < three; j++) {
            if (e.target === table.rows[i].cells[j]) {
                curRow = i;
                curCell = j;
            }
        }
    }
    if (curCell !== 0 && e.target !== specCell) {
        e.target.classList.remove('blue');
        e.target.classList.add('yellow');
    } else if (curCell === 0) {
        for (let i = 0; i < three; i++) {
            if (!table.rows[curRow].cells[i].classList.contains('yellow')) {
                table.rows[curRow].cells[i].classList.add('blue');
            }
        }
    } else if (e.target === specCell) {
        table.classList.remove('green');
        table.classList.add('yellow');
        for (let i = 0; i < three; i++) {
            for (let j = 0; j < three; j++) {
                if (!table.rows[i].cells[j].classList.contains('yellow') &&
                !table.rows[i].cells[j].classList.contains('blue') ) {
                    table.rows[curRow].cells[i].classList.add('yellow');
                }
            }
        }
    } else {
        e.preventDefault();
    }
})

const phone = document.querySelector('#phone');
const send = document.querySelector('#send');
const divError = document.querySelector('#error');
const divSuccess = document.querySelector('#success');
const nine = 9;
const thirteen = 13;

function isValid(value) {
    const arr = value.split('');
    if (arr[0] !== '+' || arr.length !== thirteen || !arr) {
        return false;
    }
    for (let i = 1; i < thirteen; i++) {
        if (!(arr[i] >= 0 && arr[i] <= nine)) {
            return false;
        }
    }
    return true;
}

phone.addEventListener('change', function() {
    divSuccess.classList.add('hidden');
    if (isValid(this.value)) {
        divError.classList.add('hidden');
    } else {
        divError.classList.remove('hidden');
    }
    if (isValid(this.value)) {
        send.removeAttribute('disabled');
    } else {
        send.setAttribute('disabled', 'disabled');
    }
});

send.addEventListener('click', function() {
    divSuccess.classList.remove('hidden');
})

