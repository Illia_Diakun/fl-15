class Employee {
    constructor(empObj) {
        this.id = empObj.id;
        this.rm_id = empObj.rm_id;
        this.name = empObj.name;
        this.performance = empObj.performance;
        this.last_vacation_date = empObj.last_vacation_date;
        this.salary = empObj.salary;
    }
}

class Dev extends Employee {
    display(parent) {
        const listItem = document.createElement('li');
        listItem.innerText = this.name;
        listItem.classList.add('dev');
        parent.appendChild(listItem);
    }
}

class RM extends Employee {
    constructor(empObj, allEmployees) {
        super(empObj);
        this.pool_name = empObj.pool_name;
        this.addChildren(allEmployees);
    }

    addChildren(employees) {
        this.children = employees.reduce((acc, emp) => {
            if (emp.rm_id === this.id) {
                const newEmployee = emp.pool_name ? new RM(emp, employees) : new Dev(emp);
                acc.push(newEmployee);
            }
            return acc;
        }, []);
    }

    display(parent) {
        const listItem = document.createElement('li');
        listItem.innerText = `${this.name} (${this.pool_name})`;
        listItem.classList.add('rm');
        const childrenList = document.createElement('ul');
        this.children.forEach((emp) => emp.display(childrenList));
        listItem.appendChild(childrenList);
        parent.appendChild(listItem);
    }
}

function readTextFile(file, callback) {
    const rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType('application/json');
    rawFile.open('GET', file, true);
    const readyStateSuccess = 4;
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === readyStateSuccess) {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

window.onload = () => {
    readTextFile('/homework/mock.json', function(text){
        const employees = JSON.parse(text);
        const topLevelRm = new RM(employees.find((emp) => !emp.rm_id), employees);
        console.log(topLevelRm);
        const root = document.getElementById('root');
        root.innerHTML = '';
        topLevelRm.display(root);
    });
};
