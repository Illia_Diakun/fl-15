const $list = $(".list");
const $input = $("#add-input");
const $add = $("#add-submit");


const todos = [
  {
    text: "Buy milk",
    done: false
  },
  {
    text: "Play with dog",
    done: true
  }
];
$(document).ready(function () {
  todos = getTodos();
  if (!todos) {
    todos = [];
    setTodos(todos);
  }
  renderList(todos);
});

$add.on('click', function () {
  const inputText = $input.val();
  if (inputText) {
    const todos = getTodos();
    todos.push({ text: inputText, done: false });
    setTodos(todos);
    renderList(todos);
  }
}); 

function getTodos() {
  return JSON.parse(localStorage.getItem('todos'));
};

function setTodos(todos) {
  localStorage.setItem('todos', JSON.stringify(todos));
};

function markDone(index) {
  const todos = getTodos();
  if (todos[index].done) {
    return;
  }
  todos[index].done = true;
  setTodos(todos);
  renderList(todos);
}

function remove(index) {
  const todos = getTodos();
  todos.splice(index, 1);
  setTodos(todos);
  renderList(todos);
}

function renderList(todos) {
  $list.empty();
  todos.forEach((todo, index) => {
    const listItem = $('<li></li>');
    listItem.addClass('item');
    const itemText = $('<span></span>').text(todo.text).addClass('item-text');
    itemText.on('click', function () {
      markDone(index);
    });
    if (todo.done) {
      itemText.addClass('done');
    }
    listItem.append(itemText);
    const button = $('<button></button>').text('Remove').addClass('item-remove');
    button.on('click', function() {
      remove(index);
    });
    listItem.append(button);
    $list.append(listItem);
  });
}

