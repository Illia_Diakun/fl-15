function maxElement(arr) {
    return Math.max(...arr);
}

function copyArray(arr) {
    return [...arr]
}

function addUniqueId(obj) {
    const newObj = Object.assign({}, obj);
    newObj.id = Symbol();
    return newObj;
}

function regroupObject(obj) {
    const {id, age, university} = obj.details;
    return { university: university, user: { age: age, firstName: obj.name, id: id } };
}

function findUniqueElements(arr) {
    return Array.from(new Set(arr));
 }

function hideNumber(str) {
    const negativeFour = -4;
    return str.slice(negativeFour).padStart(str.length, '*');
}

function add(a, b = new Error('Missing property')) {
    const two = 2;
    if (arguments.length < two) {
        return b
    } else {
        return a+b
    }
}

function getMyReposProm(url) {
    fetch(url)
        .then(response => response.json())
        .then(myInfo => {
            const myRepos = myInfo.map(item => item.name).sort();
            console.log(myRepos);
        })
        .catch(error => {
            console.log(`${error.name}: ${error.message}`);
        });
}

async function getMyReposAs(url) {
    try {
        const request = await fetch(url);
        const myInfo = await request.json();
        const myRepos = myInfo.map(item => item.name).sort();
        console.log(myRepos);
    } catch (error) {
        console.log(`${error.name}: ${error.message}`);
    }
}

